const cron = require("node-cron");
const express = require("express");
var mongoose = require('mongoose');
var myip = require('quick-local-ip');
var Server = require('./models/server');
const { exec } = require('child_process');

app = express();

mongoose.connect(process.env.MONGODB_URI || "mongodb://socialii:socialii123@ds245971.mlab.com:45971/socialii",
 {
   useNewUrlParser: true,
   useFindAndModify:true,
   useUnifiedTopology: true,
   useCreateIndex:true
 });
app.set('port', process.env.PORT || 8080);

// schedule tasks to be run on the server
cron.schedule("*/10 * * * * *", function () {
    console.log("Running Job every 10 sec");

    var servername;
    var os;
    var cpu_info;
    var uptime;
    var processes;
    var disk ;
    var date;
    var cpu_loadaverages;
    var ip = myip.getLocalIP4;
    var ipv4 = myip.getLocalIP4();
    var ipv6 =  myip.getLocalIP6();
    var storage ;

    exec('uname -a', (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        servername = stdout;
        os = stdout;
        console.log(`stderr: ${stderr}`);
      });

      exec('lscpu', (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        cpu_info = stdout;
        cpu_info.replace("\n", '. ')
        console.log(`stderr: ${stderr}`);
      });

      exec('uptime -p', (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        uptime = stdout;
        console.log(`stderr: ${stderr}`);
      });


      // djbsb

      exec('ps aux | wc -l', (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        processes = stdout;
        console.log(`stderr: ${stderr}`);
      });

      exec('df', (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`disk stdout: ${stdout}`);
        disks = stdout;
        console.log(`stderr: ${stderr}`);
      });

      exec('date', (err, stdout, stderr) => {
        if (err) {
          // node couldn't execute the command
          return;
        }
      
        // the *entire* stdout and stderr (buffered)
        console.log(`stdout: ${stdout}`);
        date = stdout;
        console.log(`stderr: ${stderr}`);
      });

    console.log(servername);
    Server.findOneAndUpdate({ 
      servername: servername,
      os:os,
      ip:ip,
      ipv4: ipv4,
      ipv6: ipv6,
      processor_info: cpu_info,
      running_process: processes,
      current_systime: date,
      sys_uptime: uptime,
      real_memory: disk,
      cpu_loadaverages : cpu_loadaverages,
      storage: storage
    }, function (err) {
        if (err) {
            return next(err);
        }else{
          var newServer = new Server({
            servername: servername,
            os:os,
            ip:ip,
            ipv4: ipv4,
            ipv6: ipv6,
            processor_info:cpu_info,
            running_process: processes,
            current_systime: date,
            sys_uptime: uptime,
            real_memory:disk,
            cpu_loadaverages: cpu_loadaverages,
            storage: storage
        });
        newServer.save();
        }
    });
});


app.listen(app.get('port'), function() {
    console.log('Running at ' + app.get('port'));
});