var mongoose = require('mongoose');

var serverSchema = mongoose.Schema({
    servername: { 
        type: String, 
        required: true,
        unique: true
    },
    ip: { 
        type: String, 
    },
    ipv4: { 
        type: String, 
    },
    ipv6: { 
        type: String, 
    },
    os: {
        type: String,
    },
    processor_info: {
        type: String,
    },
    sys_uptime: {
        type: String,
    },
    current_systime: {
        type: String,
    },
    running_process: {
        type: String,
    },
    cpu_loadaverages: {
        type: String,
    },
    real_memory: {
        type: String,
    },
    storage: {
        type: String,
        contentType: String
    },
    createdAt: {
        type: Date, default: Date.now
    }
});

var Server = mongoose.model("Server", serverSchema);

module.exports = Server;